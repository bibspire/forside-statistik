(sleep 1; touch stat.js) &
while inotifywait -e modify,close_write,move_self -q stat.js
do 
  kill -9 `cat .pid`
  sleep 0.3
  clear;
  node stat.js
  echo $! > .pid
  sleep 1
done
