# Forside-statistik

Det ser ud til, at det er forskelligt materialer der har forsider i DDB-CMS og Den Åbne Platform.

Dette er en stikprøve-undersøgelse af hvor mange forsider der er i de to platforme.

## Metode 

Metoden for undersøgelsen bliver:

1. Jeg laver liste med de 10.000 tilfældige materialer fra københavn ud fra seneste BibSpire WebTrekk-dump fra Randers.
2. Jeg tjekker om der er forside for disse materialer i den åbne platform og DDB-CMS
3. Resultatet skrives her

## Kode

Udtræk af pid'er inkl. popularitet fra webtrek-CSV fil:

```
cat ~/bibdata/webtrekk.webtrekk.202006181331.csv  | grep ting | grep bibliotek_kk_dk | sed -e 's/.*ting..*[.]//' | sed -e s/'\t.*//' | sort | grep ":" | sed -e s/"#.*//" | bzip2 -9 > pids.txt.bz2
```

Udtræk af 10.000 tilfældige sample:

```
bzcat pids.txt.bz2 | uniq | sort -R | head -n 10000 > samples.txt
```

Kør statistik:

```
npm install
node stat.js > result.log
```

## Resultater

Ud af de 10.000 tilfældige materialer der er vist på Københavns bibliotekers hjemmeside gælder følgende:

- 4.090 har ingen forside
- 3.954 har forside i både DDB-CMS og Den Åbne Platform
- 1.801 har forside i Den Åbne Platform, _men ikke i DDB-CMS_
- 155 har forside i DDB-CMS, _men ikke i Den Åbne Platform_

Så hvis man kombinerer forsider fra begge services vil man kunne øge antallet af forsider for materialerne der vises på bibliotekets hjemmeside med ca. 40%.

<small>1801 / (3954 + 155) = 43.83%, – afrundet/nedrundet til et betydende ciffer = ca. 40% (nedrundet for at give konservativt minimumsestimat, med stor statistisk sikkerhed)</small>
