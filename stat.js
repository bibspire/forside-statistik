const puppeteer = require("puppeteer");
const fs = require("fs");
const fetch = require("node-fetch");

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
const baseUrl = "https://bibliotek.kk.dk/ting/object/";
let samples = fs.readFileSync("samples.txt", "utf-8").split("\n");

const openplatformToken = "a1f6d3daece92cc81760033c4b7b9c3adc24b881";
const coverUrlUrl = (pid) =>
  `https://openplatform.dbc.dk/v3/work?pids=[%22${pid}%22]&access_token=${openplatformToken}&fields=[%22coverUrlFull%22]`;

async function main() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  let count = { none: 0, ddbcms: 0, openplatform: 0, both: 0, total: 0 };
  for (let i = 0; i < samples.length; ++i) {
    try {
      const pid = samples[i];
      const url = baseUrl + pid;

      await page.goto(url);
      let openplatformCover = await (await fetch(coverUrlUrl(pid))).json();
      openplatformCover = openplatformCover.data[0].coverUrlFull;
      openplatformCover = openplatformCover && openplatformCover[0];

      // sleep to give DDB-CMS time to load the image
      await sleep(2000);
      let ddbcmsCover;
      ddbcmsCover = await page.evaluate(() => {
        const elem = document.querySelector(".ting-cover img");
        return elem && elem.src;
      });
      if (!openplatformCover && !ddbcmsCover) count.none++;
      if (openplatformCover && !ddbcmsCover) {
        count.openplatform++;
        console.log("openplatform", pid);
      }
      if (!openplatformCover && ddbcmsCover) {
        count.ddbcms++;
        console.log("ddbcms", pid);
      }
      if (openplatformCover && ddbcmsCover) count.both++;
      count.total++;
      console.log(count);
    } catch(e) {
      console.log(e)
    }
  }
  await browser.close();
}
main();
